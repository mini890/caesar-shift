import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

/**
 * A GUI that use the Caesar class to encode/decode messages
 */
public class CaesarGui extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;

    // the CheckBox to select the number of shifts
    private JRadioButton[] shiftRadio;
    // the Caesar class to encode/decode
    private Caesar caesar;

    // The message to encode
    private JTextField clearTextIn;
    private JLabel encodedTextOut;
    // the message to decode
    private JTextField codedTextIn;
    private JLabel decodedTextOut;

    /**
     * Constructor
     */
    CaesarGui() {
        super("Caesar encoding/decoding by Mini890");
        // we will use a GridLayout to store our GUI component
        JPanel mainPanel = new JPanel(new GridLayout(1, 2, 4, 4));

        // to store the coded text input output fiels
        JPanel textPanel = new JPanel(new GridLayout(14, 1));
        // a little gap at the beginning of the Panel
        textPanel.add(new JLabel());

        // text to encode header
        textPanel.add(createCenteredLabel("Enter text to encode here"));
        // the JTextField to enter the text to encode
        clearTextIn = new JTextField(30);
        // with a documentListener to be informed when the text changes
        clearTextIn.getDocument().addDocumentListener(new ClearListener());
        textPanel.add(clearTextIn);
        // the header for the coded text
        textPanel.add(createCenteredLabel("Coded Text"));
        // and the JLabel to display it
        encodedTextOut = createOutputLabel();
        textPanel.add(encodedTextOut);

        // same thing for the string to decode
        textPanel.add(new JLabel());
        textPanel.add(createCenteredLabel("Enter text to decode here"));
        // the JTextField to enter the coded text with its listener
        codedTextIn = new JTextField(30);
        codedTextIn.getDocument().addDocumentListener(new CodedListener());
        textPanel.add(codedTextIn);
        textPanel.add(createCenteredLabel("Decoded text"));
        decodedTextOut = createOutputLabel();
        textPanel.add(decodedTextOut);

        // the left panel constains a lot less lines than the right panel
        // will make a GridLayout(2,1) with empty bottom
        JPanel leftPanel = new JPanel(new GridLayout(2,1));
        leftPanel.add(textPanel);
        leftPanel.add(new JLabel());
        mainPanel.add(leftPanel);


        // init the number of shifts to 1
        caesar = new Caesar(1);
        // creates the right panel with all the possible shifts
        JPanel shiftPanel = createShiftPanel();
        shiftRadio[1].setSelected(true);
        mainPanel.add(shiftPanel);

        // the panel stored in the center of the JFrame
        add(mainPanel, BorderLayout.CENTER);

        // standard operation to show the JFrame
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLocation(30, 30);
        pack();
        setVisible(true);
    }

    /**
     * A radio button to change the shifts has been selected
     */
    public void actionPerformed(ActionEvent e) {
        // get the radio button that fired the event
        Object o = e.getSource();
        // scan the list to find which one
        for(int i = 0; i < shiftRadio.length; i++) {
            // when found
            if(o == shiftRadio[i]) {
                // set the Caesar with this new value
                caesar.setShiftValue(i);
                // exit the loop
                break;
            }
        }
        // update both strings to code and decode with the new shift
        updateCodedString();
        updateDecodedString();
    }

    /**
     * A method to create a JLabel with foreground color Blue and with text centered
     */
    private JLabel createCenteredLabel(String text) {
        JLabel label = new JLabel(text);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setForeground(Color.BLUE);
        return label;
    }

    /**
     * A method to create the output labels with White background and a border
     */
    private JLabel createOutputLabel() {
        JLabel label = new JLabel();
        label.setOpaque(true);
        label.setBackground(Color.WHITE);
        label.setBorder(BorderFactory.createLineBorder(Color.RED));
        return label;
    }
    /**
     * To update coded string
     */
    private void updateCodedString() {
        // get the text from the JTextField
        String line = clearTextIn.getText();
        // set the label containing the text out with the new conversion
        encodedTextOut.setText(caesar.encode(line));
    }

    /**
     * To update decoded string
     */
    private void updateDecodedString() {
        // get the text from the JTextField
        String line = codedTextIn.getText();
        // set the label containing the decoded string
        decodedTextOut.setText(caesar.decode(line));
    }

    /**
     * To create the JPanel containing the possible shifts
     */
    private JPanel createShiftPanel() {
        // a double alphabet we will display the 26 letters from 0, 1, 2, 3...
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ";
        // a Panel for the 26 possible shifts plus a header
        JPanel panel = new JPanel(new GridLayout(27, 1));
        panel.setBorder(BorderFactory.createLineBorder(Color.BLUE));

        // the header atthe top of the Panel
        JLabel label = new JLabel("Number of letters for the shift");
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setForeground(Color.BLUE);
        panel.add(label);

        // a ButtonGroup only one radioButton can be selected at each time
        ButtonGroup bg = new ButtonGroup();
        shiftRadio = new JRadioButton[26];
        // add the 26 JRadioButton
        for(int i = 0; i < 26; i++) {
            // format its text 00 - ABCDEF...
            String str = String.format("%02d - %s", i, alphabet.substring(i, i+26));
            shiftRadio[i] = new JRadioButton(str);
            shiftRadio[i].addActionListener(this);
            // add it to the ButtonGroup
            bg.add(shiftRadio[i]);
            // and to the panel
            panel.add(shiftRadio[i]);
        }
        return panel;
    }

    /**
     * To start the GUI
     */
    public static void main(String[] args) {
        new CaesarGui();
    }

    /**
     * A listener to be informed whenever the JTextField for the coded string is changed
     */
    class CodedListener implements DocumentListener {

        @Override
        public void changedUpdate(DocumentEvent arg0) {
            updateDecodedString();
        }

        @Override
        public void insertUpdate(DocumentEvent arg0) {
            updateDecodedString();
        }

        @Override
        public void removeUpdate(DocumentEvent arg0) {
            updateDecodedString();
        }
    }

    /**
     * A listener to be informed whenever the JTextField of the clear text is changed
     */
    class ClearListener implements DocumentListener {
        @Override
        public void changedUpdate(DocumentEvent arg0) {
            updateCodedString();
        }

        @Override
        public void insertUpdate(DocumentEvent arg0) {
            updateCodedString();
        }

        @Override
        public void removeUpdate(DocumentEvent arg0) {
            updateCodedString();
        }

    }

}