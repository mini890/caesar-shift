import java.util.Scanner;
/*
 * The first registered secret code that was used by Julius Caesar (or at least his army)
 * during the Gaul war 50 BC
 * All letters in the alphabet are shifted by a certain number of letters
 * The number of shifts must be known by the sender and the receiver
 * So if shift == 1
 * abcdefghijklmnopqrstuvwxyz will be coded as
 * BCDEFGHIJKLMNOPQRSTUVWXYZA
 * if shift == 5
 * abcdefghijklmnopqrstuvwxyz will be coded as
 * FGHIJKLMNOPQRSTUVWXYZABCDE
 */
public class Caesar {
    // the number of letter(s) we shifted
    private int nbShift;

    /**
     *  constructor that receives the number of letters we will shift
     * @param nbShift
     */
    public Caesar(int nbShift) {
        setShiftValue(nbShift);
    }

    /**
     * To set the initial or change the shift value
     * @param nbShift
     */
    public void setShiftValue(int nbShift) {
        // wrap around shift > 26 and accept only positive shifts
        if(nbShift < 0)
            nbShift = -nbShift;
        // store the number of shift to be applied
        this.nbShift = nbShift % 26;
    }

    /**
     * this method returns the encrypted String from the String received as parameter
     * according to the current number of letter(s) to shift in the alphabet
     */
    public String encode(String clear) {
        // ignore if null
        if(clear == null)
            return "";
        // by convention in the cryptology world all original messages are in lower case
        // and coded messages in uppercase
        // so let us put the original message in lower cases
        // convert the original message into an array of char
        // into which converted character will be put later
        char[] digit = clear.toLowerCase().toCharArray();
        // pass through all input digits to convert them
        for(int i = 0; i < digit.length; i++) {
            // if they are not between 'a' and 'z' just ignore them (message has been translated to lowercase)
            if(digit[i] < 'a' || digit[i] > 'z')
                continue;

            // convert the digit to uppercase (as we mentionned this is a standard cryptographic convention)
            digit[i] = Character.toUpperCase(digit[i]);
            // applied the shift to it
            digit[i] += nbShift;
            // wrap around if over 'Z'
            if(digit[i] > 'Z') {
                digit[i] -= 'Z';
                digit[i] += ('A' - 1);
            }
        }
        // return a new String made of these shifted characters
        return new String(digit);
    }

    /**
     * this method returns the decrypted String from the String received as parameter
     * according to the current number of letter(s) to shift in the Caesar code
     */
    public String decode(String coded) {
        // ignore if null
        if(coded == null)
            return "";
        // by convention in the cryptology world all original messages are in lowercode
        // and coded messages in uppercase
        // so let us put the original message in upper cases
        // convert the original message into an array of char
        char[] digit = coded.toUpperCase().toCharArray();
        // pass through all input coded digits to convert them
        for(int i = 0; i < digit.length; i++) {
            // if they are not between 'A' and 'Z' just ignore them (message has been translated to upperrcase)
            if(digit[i] < 'A' || digit[i] > 'Z') {
                continue;
            }
            // convert the digit to lowerrcase (as we mentionned this is a standard cryptographic convention)
            digit[i] = Character.toLowerCase(digit[i]);
            // applied the shift to it
            digit[i] -= nbShift;
            // wrap around if before 'A'
            if(digit[i] < 'a') {
                digit[i] += 'z';
                digit[i] -= ('a' - 1);
            }
        }
        // return a new String made of these shifted characters
        return new String(digit);
    }

    /**
     *  the main() method to test our Caesar class
     *  @param args
     */
    public static void main(String[] args) {

        //--------------------------------------------------------
        // Automatic tests
        //--------------------------------------------------------

        // to test with all possible shifts
        // the following line can be removed (or commented) in production code
        testAllShifts();	// <---- to be removed in  production code

        //--------------------------------------------------------
        // End of Automatic tests
        //--------------------------------------------------------

        // now test a real case
        // Creates a Scanner to read use input
        Scanner scan = new Scanner(System.in);
        // read the number of shift
        // init number of shift to an integer valid value
        int nbShift = 0;
        // loop until we break for a valid value
        while(true) {
            System.out.print("Enter number of letters to shift: ");
            String str = scan.nextLine();
            // try to translale String inputed as Integer
            try {
                nbShift = Integer.parseInt(str);
                // if the catch case was not invoked it means this is a valid integer input
                // so we break out the loop
                break;
            }
            catch(Exception e) {
                System.out.println("Sorry your nbumber of shifts is invalid");
            }
        }

        // now that we have a valid shift create a Caesar object with the number of shifts
        Caesar caesar = new Caesar(nbShift);
        // input the line to be coded
        System.out.print("Enter message to be coded: ");
        String clearMsg = scan.nextLine();
        // code the user input
        String codedString = caesar.encode(clearMsg);
        // print the coded string
        System.out.println("The coded String is:         " + codedString);
        // and its translation decoded
        System.out.println("And its translation back is: " + caesar.decode(codedString));
        scan.close();
    }

    /**
     * Used to test with all the letters of the alphabet that all the shifts work
     */
    private static void testAllShifts() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        // test the 26 possible shifts... and a little bit more just to make sure that
        // our modulo operation works correctly
        System.out.println("             " + alphabet);
        for(int i = 0; i < 30; i++) {
            Caesar caesar = new Caesar(i);
            System.out.format("Shift: %02d -> %s\n", i, caesar.encode(alphabet));
        }
        System.out.println();
    }
}
